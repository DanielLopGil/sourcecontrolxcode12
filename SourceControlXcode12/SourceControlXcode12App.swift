//
//  SourceControlXcode12App.swift
//  SourceControlXcode12
//
//  Created by Daniel Lopez on 4/27/21.
//

import SwiftUI

@main
struct SourceControlXcode12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
