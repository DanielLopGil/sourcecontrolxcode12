//
//  ContentView.swift
//  SourceControlXcode12
//
//  Created by Daniel Lopez on 4/27/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world! Xcode 12 branch")
            .padding()
            .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
